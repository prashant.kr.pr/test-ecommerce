var passport = require('passport');
var User = require('../models/user');
var LocalStrategy = require('passport-local').Strategy;

/**
 * serializeUser() -- This will basically tell the passport how to store the user in the session.
 */
passport.serializeUser(function(user, done) {
    /**
     * This will basically tell the user that if you want to save the user in the session the save it via id.
     */
    done(null, user.id);
});

passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
        done(err, user);
    });
});

passport.use('local-signup', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
}, function(req, email, password, done) {
    /**
     * Having the issue "expressValidator is not a function"
     */
    // req.checkBody('email', 'Invalid Email').notEmpty().isEmail();
    // req.checkBody('password', 'Invalid Password').notEmpty().isLength({min:4});
    // var errors = req.validationErrors();
    // if(errors) {
    //     var messages = [];
    //     errors.forEach(function(error) {
    //         messages.push(error.msg);
    //     });
    //     return done(null, false, req.flash('error', messages));
    // }
    /**
     * ----- End ------
     */

    User.findOne({'email':email}, function(err, user) {
        if(err) {
            return done(err);
        }
        if(user) {
            return done(null, false, {message: 'Email is alredy in use. '})
        }
        var newUser = new User();
        newUser.email = email;
        newUser.password = newUser.encryptPassword(password);
        newUser.save(function(err, result) {
            if(err) {
                return done(err);
            }
            return done(null, newUser);
        });
    });
}));
/**
 * Here we have created the logic for localStrategy for login. We are also validating the password here.
 */
passport.use('local-signin', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
}, function(req, email, password, done) {
    /**
     * Validation will go here.
     */
    User.findOne({'email':email}, function(err, user) {
        if(err) {
            return done(err);
        }
        if(!user) {
            return done(null, false, {message:'User does not exist.'});
        }
        if(!user.validPassword(password)) {
            return done(null, false, {message:'Invalid password.'});
        }
        return done(null, user);
    });
}));